$(document).ready(function () {
    var id = parseInt($('#id').val());
    $('a[href="#comments"]').click(function () {
        getComments(id);
    });
    $('#form_send_comment').submit(function (e) {
        e.preventDefault();
        addComment(id, $(this));
    });
    $('#form_add_article').submit(function (e) {
        e.preventDefault();
        addArticle($(this));
    });
    $('#form_change_article').submit(function (e) {
        e.preventDefault();
        changeArticle($(this));
    });
    $('.form_change_comment').submit(function (e) {
        e.preventDefault();
        changeComment($(this));
    });
    $('a[href="#delete-article"]').click(function (e) {
        e.preventDefault();
        delArticle($(this).data('delete'));
    });
    $('a[href="#delete-comment"]').click(function (e) {
        e.preventDefault();
        delComment($(this).data('delete'));
    });
});

function getComments(id) {
    $.ajax({
        type: 'POST',
        url: '/main/get_comments',
        data: 'id=' + id,
        dataType: "json",
        success: function (data) {
            var comments = '';

            for (var i = 0; i < data.length; i++) {
                comments +=
                    '<div class="media">' +
                    '<div class="media-body">' +
                    '<h4 class="media-heading">' + data[i].name + '</h4>' +
                    data[i].content + '</div>' +
                    '</div>';
            }
            $('#comments').html(comments);
        },
        error: function (data) {
            alert('error');
        }

    });
}

function addComment(id, form) {
    var date = new Date();
    $.ajax({
        type: 'POST',
        url: '/main/add_comments',
        data: $(form).serialize() + '&date=' + date + '&id=' + id,
        dataType: "json",
        success: function (data) {
            getComments(id);
        },
        error: function (data) {
            alert('error');
        }

    });
}

function addArticle(form) {
    $.ajax({
        type: 'POST',
        url: '/admin/add_article',
        data: $(form).serialize(),
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            alert('error');
        }

    });
}

function delArticle(id) {
    id = parseInt(id);
    $.ajax({
        type: 'POST',
        url: '/admin/remove_article',
        data: 'id=' + id,
        dataType: "json",
        success: function (data) {
            location.pathname = '/admin';
        },
        error: function (data) {
            alert('error');
        }

    });
}

function changeArticle(form) {
    $.ajax({
        type: 'POST',
        url: '/admin/change_article',
        data: $(form).serialize(),
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            alert('error');
        }

    });
}

function delComment(id) {
    id = parseInt(id);
    $.ajax({
        type: 'POST',
        url: '/admin/remove_comment',
        data: 'id=' + id,
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            alert('error');
        }

    });
}

function changeComment(form) {
    $.ajax({
        type: 'POST',
        url: '/admin/change_comment',
        data: $(form).serialize(),
        dataType: "json",
        success: function (data) {
            location.reload();
        },
        error: function (data) {
            alert('error');
        }

    });
}