<?php

namespace cfg;

class DB {
    private $host = 'localhost';
    private $db_name = 'testdb';
    private $login = 'testuser';
    private $password = 'admin123';
    private $port = '5432';
    public $db_connect;

    public function connectDB() {
        $this->host;
        $this->db_connect = pg_connect("host=". $this->host ." port=" . $this->port . " dbname=" . $this->db_name .
            " user=" . $this->login . " password=" . $this->password) or die('Не получается приконнектится: ' . pg_last_error());
    }
    public function disconnectDB() {
        pg_close($this->db_connect);
    }
}