<header>
    <div class="container hidden-r-b">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/">Главная</a></li>
            <li><a href="/admin">Админка</a></li>
        </ul>
    </div>
</header>
<div class="h1-block">
    <div class="container hidden-r-b">
        <h1>Главная страница</h1>
    </div>
</div>

<div class="container hidden-r-b content">
    <?php

    $article = $data['result'];
    echo '<input type="text" name="id" id="id" value="' . $article['id'] . '" style="display: none">';

    echo '<div class="panel panel-default">';
    echo '<div class="panel-heading"><h4>' . $article['name'] . '</h4></div>';
    echo '<div class="panel-body">' . $article['content'] . '</div>';
    echo '</div>';

    ?>


    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#list_discipline"
                   href="#comments"><span class="glyphicon glyphicon-chevron-down glyphicon-padding"></span> Комментарии</a>
            </h4>
        </div>
        <div id="comments" class="panel-collapse collapse comments">
            <div class="panel-body"></div>
        </div>
    </div>
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#list_discipline"
                   href="#add_comment"><span class="glyphicon glyphicon-chevron-down glyphicon-padding"></span> Добавить комментарий</a>
            </h4>
        </div>
        <div id="add_comment" class="panel-collapse collapse comments">
            <div class="panel-body">
                <form id="form_send_comment">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
                        <input type="text" name="name" class="form-control" placeholder="Никнейм">
                        <textarea class="form-control no-tb-border" name="content" rows="10"></textarea>
                        <input type="submit" class="form-control" value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>