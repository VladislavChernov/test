<header>
    <div class="container hidden-r-b">
        <ul class="nav nav-tabs">
            <li class="active"><a href="/">Главная</a></li>
            <li><a href="/admin">Админка</a></li>
        </ul>
    </div>
</header>
<div class="h1-block">
    <div class="container hidden-r-b">
        <h1>Главная страница</h1>
    </div>
</div>

<div class="container hidden-r-b content">
    <?php

    foreach ($data['result'] as $article) {
        echo '<div class="panel panel-default">';
        echo '<div class="panel-heading"><h4><a href="/main/article/' . $article['id'] . '">' . $article['name'] . '</a></h4></div>';
        $string = strip_tags($article['content']);
        $string = substr($string, 0, 100);
        $string = rtrim($string, "!,.-");
        $string = substr($string, 0, strrpos($string, ' '));

        echo '<div class="panel-body">' . $string . '...</div>';
        echo '</div>';
    }

    ?>
</div>