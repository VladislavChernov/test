<header>
    <div class="container hidden-r-b">
        <ul class="nav nav-tabs">
            <li><a href="/">Главная</a></li>
            <li class="active"><a href="/admin">Админка</a></li>
        </ul>
    </div>
</header>
<div class="h1-block">
    <div class="container hidden-r-b">
        <h1>Административная панель</h1>
    </div>
</div>

<div class="container hidden-r-b content">
    <div class="panel panel-default">
        <div class="panel-heading"><h4>Материалы</h4></div>
        <div class="panel-body">
            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Дата изменения</th>
                    <th>Опубликовано?</th>
                    <th>Заголовок</th>
                    <th>Удалить</th>
                </tr>

                <?php
                foreach ($data['result'] as $article) {
                    $bool = 'Нет';
                    if ($article['status'] == 't')
                        $bool = 'Да';
                    echo '
                    <tr>
                        <td>' . $article['id'] . '</td>
                        <td>' . $article['date'] . '</td>
                        <td>' . $bool . '</td>
                        <td><a href="/admin/article/' . $article['id'] . '">' . $article['name'] . '</a></td>
                        <td><a href="#delete-article" data-delete="' . $article['id'] . '">Удалить</a></td>
                    </tr>';
                }

                ?>

            </table>
        </div>
    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#list_discipline"
                   href="#add_article"><span class="glyphicon glyphicon-chevron-down glyphicon-padding"></span> Добавить
                    статью</a>
            </h4>
        </div>
        <div id="add_article" class="panel-collapse collapse comments">
            <div class="panel-body">
                <form id="form_add_article">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-plus"></span></span>
                        <input type="text" name="name" class="form-control" placeholder="Название">
                        <label class="form-control" style="border-top: none" for="status">Опубликовано: <input type="checkbox" name="status"></label>
                        <textarea class="form-control no-tb-border" name="content" rows="10"></textarea>
                        <input type="submit" class="form-control" value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>