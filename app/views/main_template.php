<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title><?php echo $data['page_name']; ?></title>

    <link rel="stylesheet" type="text/css" href="/static/css/glyphicons.css"/>
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Philosopher:400,400i,700,700i&amp;subset=cyrillic"
          rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/static/css/style.css"/>

    <script type="text/javascript" src="/static/js/jquery.js"></script>
    <script type="text/javascript" src="/static/js/bootstrap.js"></script>
    <script type="text/javascript" src="/static/js/script.js"></script>
</head>
<body>
<?php include 'app/views/' . $content_view; ?>
</body>
</html>