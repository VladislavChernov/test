<header>
    <div class="container hidden-r-b">
        <ul class="nav nav-tabs">
            <li><a href="/">Главная</a></li>
            <li class="active"><a href="/admin">Админка</a></li>
        </ul>
    </div>
</header>
<div class="h1-block">
    <div class="container hidden-r-b">
        <h1>Административная панель</h1>
    </div>
</div>

<div class="container hidden-r-b">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Редактирование статьи / <a href="#delete-article"
                                                               data-delete="<?php echo $data['result'][0]['id']; ?>">Удалить</a>
            </h4>
        </div>
        <div id="change_article" class="panel-body comments">
            <div class="panel-body">
                <form id="form_change_article">
                    <div class="input-group">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-plus"></span></span>
                        <input type="text" name="id" value="<?php echo $data['result'][0]['id']; ?>"
                               style="display: none">
                        <input type="text" name="name" class="form-control" placeholder="Название"
                               value="<?php echo $data['result'][0]['name']; ?>">
                        <label class="form-control" style="border-top: none" for="status">Опубликовано: <input
                                    type="checkbox" name="status"
                                <?php
                                if ($data['result'][0]['status'] == 't')
                                    echo 'checked';
                                ?>
                            ></label>
                        <textarea class="form-control no-tb-border" name="content"
                                  rows="10"><?php echo $data['result'][0]['content']; ?></textarea>
                        <input type="submit" class="form-control" value="Отправить">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
    if (!empty($data['result']['comments'])) {
        foreach ($data['result']['comments'] as $comment) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title"><a data-toggle="collapse" data-parent="#list_discipline"
                                               href="#change_comment_<?php echo $comment['id']; ?>"><span
                                    class="glyphicon glyphicon-chevron-down glyphicon-padding"></span> Редактирование
                            комментария</a> id_<?php echo $comment['id']; ?> / <a href="#delete-comment"
                                                                                  data-delete="<?php echo $comment['id']; ?>">Удалить</a>
                    </h4>
                </div>
                <div id="change_comment_<?php echo $comment['id']; ?>" class="panel-collapse collapse comments">
                    <div class="panel-body">
                        <form id="form_change_comment_<?php echo $comment['id']; ?>" class="form_change_comment">
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-comment"></span></span>
                                <input type="text" name="id" value="<?php echo $comment['id']; ?>"
                                       style="display: none">
                                <input type="text" name="name" class="form-control" placeholder="Название"
                                       value="<?php echo $comment['name']; ?>">
                                <textarea class="form-control no-tb-border" name="content"
                                          rows="10"><?php echo $comment['content']; ?></textarea>
                                <input type="submit" class="form-control" value="Отправить">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        <?php }
    } ?>
</div>
