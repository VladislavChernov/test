<?php

use cfg\DB;

class Model_Main extends Model {
    public function get_data() {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT id, name, content, date FROM article WHERE status ORDER BY id DESC';
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        return $res;
    }

    public function get_Article($id) {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT id, name, content, date FROM article WHERE status AND id=' . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        return $res[0];
    }

    public function get_Comment_Data() {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT id, name, content, date FROM comment WHERE article_id=' . $_POST['id'];
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function add_Comment() {
        $db = new DB();
        $db->connectDB();

        $id = Model_Main::clean($_POST['id']);
        $name = Model_Main::clean($_POST['name']);
        $content = Model_Main::clean($_POST['content']);

        $query = "INSERT INTO comment (article_id, name, content) VALUES (" . $id . ", '" .
            $name . "', '" . $content . "')";
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    function clean($value = '') {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }
}