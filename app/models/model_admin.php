<?php

use cfg\DB;

class Model_Admin extends Model {
    public function get_data() {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT id, name, content, status, date FROM article ORDER BY id DESC';
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        return $res;
    }

    public function get_Article($id) {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT * FROM article WHERE id=' . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $query = 'SELECT * FROM comment WHERE article_id=' . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res['comments'] = pg_fetch_all($result);

        $db->disconnectDB();
        return $res;
    }

    public function get_Comment_Data() {
        $db = new DB();
        $db->connectDB();

        $query = 'SELECT id, name, content, date FROM comment';
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function add_Article() {
        $db = new DB();
        $db->connectDB();

        $status = 'FALSE';
        $name = $content = '';

        if (isset($_POST['name']))
            $name = Model_Admin::clean($_POST['name']);
        if (isset($_POST['content']))
            $content = Model_Admin::clean($_POST['content']);
        if (isset($_POST['status']))
            $status = Model_Admin::clean($_POST['status']);
        $date = 'now()';

        if ($status == 'on')
            $status = 'TRUE';

        $query = "INSERT INTO article (name, content, date, status) VALUES ('" . $name . "', '" . $content . "', " . $date . ", " . $status . ")";
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function delete_Article() {
        $db = new DB();
        $db->connectDB();
        $id = '-1';

        if (isset($_POST['id']))
            $id = Model_Admin::clean($_POST['id']);

        $query = "DELETE FROM article WHERE id=" . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $query = "DELETE FROM comment WHERE article_id=" . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function delete_Comment() {
        $db = new DB();
        $db->connectDB();
        $id = '-1';

        if (isset($_POST['id']))
            $id = Model_Admin::clean($_POST['id']);


        $query = "DELETE FROM comment WHERE id=" . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function change_Article() {
        $db = new DB();
        $db->connectDB();

        $status = 'FALSE';
        $id = '-1';
        $name = $content = '';

        if (isset($_POST['id']))
            $id = Model_Admin::clean($_POST['id']);
        if (isset($_POST['name']))
            $name = Model_Admin::clean($_POST['name']);
        if (isset($_POST['content']))
            $content = Model_Admin::clean($_POST['content']);
        if (isset($_POST['status']))
            $status = Model_Admin::clean($_POST['status']);
        $date = 'now()';

        if ($status == 'on')
            $status = 'TRUE';

        $query = "UPDATE article  SET name='" . $name . "', content='" . $content . "', date=" . $date .
            ", status=" . $status . " WHERE id=" . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    public function change_Comment() {
        $db = new DB();
        $db->connectDB();

        $id = '-1';
        $name = $content = '';

        if (isset($_POST['id']))
            $id = Model_Admin::clean($_POST['id']);
        if (isset($_POST['name']))
            $name = Model_Admin::clean($_POST['name']);
        if (isset($_POST['content']))
            $content = Model_Admin::clean($_POST['content']);
        $date = 'now()';


        $query = "UPDATE comment SET name='" . $name . "', content='" . $content . "', date=" . $date .
            " WHERE id=" . $id;
        $result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());
        $res = pg_fetch_all($result);

        $db->disconnectDB();
        echo json_encode($res);
    }

    function clean($value = '') {
        $value = trim($value);
        $value = stripslashes($value);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);

        return $value;
    }
}