<?php


class Controller_Admin extends Controller {
    function __construct() {
        $this->model = new Model_Admin();
        $this->view = new View();
    }

    function action_index() {
        $data['result'] = $this->model->get_data();
        $data['page_name'] = 'Админка';

        $this->view->generate('admin_view.php', 'main_template.php', $data);
    }

    function action_article($argument) {
        $data['result'] = $this->model->get_Article($argument);
        $data['page_name'] = $data['result'][0]['name'] . ' | Статья | Админка';

        $this->view->generate('admin_article_view.php', 'main_template.php', $data);
    }

    function action_get_comments() {
        $data['result'] = $this->model->get_Comment_Data();
        return $data;
    }

    function action_add_article() {
        $data['result'] = $this->model->add_Article();
    }

    function action_remove_article() {
        $data['result'] = $this->model->delete_Article();
    }

    function action_remove_comment() {
        $data['result'] = $this->model->delete_Comment();
    }

    function action_change_article() {
        $data['result'] = $this->model->change_Article();
    }

    function action_change_comment() {
        $data['result'] = $this->model->change_Comment();
    }
}