<?php


class Controller_Main extends Controller {
    function __construct() {
        $this->model = new Model_Main();
        $this->view = new View();
    }

    function action_index() {
        $data['result'] = $this->model->get_data();
        $data['page_name'] = 'Главная страница';

        $this->view->generate('main_view.php', 'main_template.php', $data);
    }

    function action_article($argument) {
        $data['result'] = $this->model->get_Article($argument);
        $data['page_name'] = $data['result']['name'] . ' | Статья';

        $this->view->generate('main_article_view.php', 'main_template.php', $data);
    }

    function action_get_comments() {
        $data['result'] = $this->model->get_Comment_Data();
        return $data;
    }

    function action_add_comments() {
        $data['result'] = $this->model->add_Comment();
        return $data;
    }
}