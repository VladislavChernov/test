'use strict';

var gulp = require('gulp'),
    mainFiles = require('main-bower-files'),
    wiredep = require('wiredep').stream,
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    csscomb = require('gulp-csscomb'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat');

var pathSource = '../static-src/';
var pathDebug = '../static/';

gulp.task('bower-main-js', function () {
    return gulp.src(mainFiles('**/*.js'))
        .pipe(gulp.dest(pathDebug + 'js/'));
});

gulp.task('bower-main-css', function () {
    return gulp.src(mainFiles('**/*.css'))
        .pipe(gulp.dest(pathDebug + 'css/'));
});

gulp.task('bower-main-font', function () {
    var target = [
        '**/*.otf', '**/*.svg', '**/*.ttf',
        '**/*.woff', '**/*.woff2'
    ];

    return gulp.src(mainFiles(target))
        .pipe(gulp.dest(pathDebug + 'fonts/'));
});

gulp.task('bower-wiredep', function () {
    var target = gulp.src('../app/views/main_template.php');

    return target.pipe(wiredep({
        fileTypes: {
            html: {
                replace: {
                    js: function (filePath) {
                        var fileName = filePath.match('^(.+)/([^/]+)$');
                        return '<script type="text/javascript" src="{% static \'js\/' + fileName[2] + '\' %}"></script>';
                    },
                    css: function (filePath) {
                        var fileName = filePath.match('^(.+)/([^/]+)$');
                        return '<link rel="stylesheet" type="text/css" href="{% static \'css\/' + fileName[2] + '\' %}"/>';
                    }
                }
            }
        }
    })).pipe(gulp.dest('../app/views/'));
});

gulp.task('work-with-js', function () {
    var target = gulp.src(pathSource + 'scripts/**/*.js');

    return target.pipe(concat('script.js'))
        .pipe(gulp.dest(pathDebug + '/js/'));
});

gulp.task('work-with-css', function () {
    var target = gulp.src(pathSource + 'styles/style.sass');

    return target.pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(autoprefixer())
        .pipe(csscomb())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest(pathDebug + '/css/'));
});

gulp.task('watch', function () {
    gulp.watch(
        pathSource + 'scripts/**/*.js',
        [ 'work-with-js' ]
    );
    gulp.watch(
        pathSource + 'styles/**/*.sass',
        [ 'work-with-css' ]
    );
});

gulp.task('default', [
    'bower-main-js',
    'bower-main-css',
    'bower-main-font',
    'bower-wiredep',
    'work-with-js',
    'work-with-css',
    'watch'
]);